# Project Title

Programa de Estágio Red Hat Brasília

Tarefa da Semana 2 

### Installing

Clonar com SSH:
git@gitlab.com:ggianini_rh/stag-presentation.git


Clonar com HTTPS: (Requer autenticação)
https://gitlab.com/ggianini_rh/stag-presentation.git

## How to use

Basta clonar a página HTML

## Authors

* **Gustavo Gianini** - (https://gitlab.com/ggianini_rh/)
* **Jessica Mazoni** - (https://gitlab.com/jessicamazoni/)
